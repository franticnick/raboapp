
/*
 * Copyright (c) 2018 Bandiago
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bandiago.apps.raboapp.repository.reader

import android.content.Context
import android.util.Log
import com.bandiago.apps.raboapp.R
import com.opencsv.CSVReader
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader


/**
 * Csv Reader
 */
class IssuesCsvReader(private val context: Context) {

    /**
     * read issues from csv file and return as list
     */
    suspend fun getIssues():  List<Array<String>> {
        var result =  mutableListOf<Array<String>>()
        withContext(Dispatchers.Default) {
            result = readCsv()
        }

        return result
    }


    private fun readCsv(): MutableList<Array<String>> {
        var reader: InputStreamReader? = null
        var csvReader: CSVReader? = null
        try {
            reader = context.resources.openRawResource(R.raw.issues).reader()
            csvReader = CSVReader(BufferedReader(reader))

            val csvList = csvReader.readAll()
            reader.close()
            csvReader.close()

            return csvList
        } catch (e: Exception) {
            Log.e("can't parse CSV file", e.message)
        } finally {
            if (reader != null) {
                try {
                    reader.close()
                } catch (e: IOException){
                    Log.e("exception: ", e.message)
                }
            }

            if (csvReader != null) {
                try {
                    csvReader.close()
                } catch (e: IOException){
                    Log.e("exception: ", e.message)
                }
            }
        }

        return mutableListOf()
    }

}