/*
 * Copyright (c) 2018 Bandiago
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bandiago.apps.raboapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bandiago.apps.raboapp.R
import com.bandiago.apps.raboapp.adapter.dto.IssueDto
import java.text.SimpleDateFormat
import java.util.*

/**
 * Adapter Class
 */
class IssuesAdapter : RecyclerView.Adapter<IssuesAdapter.ViewHolder>() {

    private var readDateFormat = SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss", Locale.ENGLISH)
    private val showDateFormat = SimpleDateFormat.getDateInstance()!!

    var mItems: ArrayList<IssueDto> = ArrayList()

    var mTitleList = ArrayList<String>()

    class ViewHolder(itView: View) : RecyclerView.ViewHolder(itView) {
        val tvTitleFirst = itView.findViewById<TextView>(R.id.titleFirst)!!
        val tvFirst = itView.findViewById<TextView>(R.id.first)!!

        val tvTitleSecond = itView.findViewById<TextView>(R.id.titleSecond)!!
        val tvSecond = itView.findViewById<TextView>(R.id.second)!!

        val tvTitleThird = itView.findViewById<TextView>(R.id.titleThird)!!
        val tvThird = itView.findViewById<TextView>(R.id.third)!!

        val tvTitleFourth = itView.findViewById<TextView>(R.id.titleFourth)!!
        val tvFourth = itView.findViewById<TextView>(R.id.fourth)!!
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v: CardView =
            LayoutInflater.from(parent.context).inflate(R.layout.issues_content, parent, false) as CardView

        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemDto = mItems[position]

        holder.tvTitleFirst.text = mTitleList[0]
        holder.tvTitleSecond.text = mTitleList[1]
        holder.tvTitleThird.text = mTitleList[2]
        holder.tvTitleFourth.text = mTitleList[3]

        with(itemDto) {
            holder.tvFirst.text = v1
            holder.tvSecond.text = v2
            holder.tvThird.text = v3
            holder.tvFourth.text = showDateFormat.format(readDateFormat.parse(v4))
        }

    }

    override fun getItemCount(): Int {
        return mItems.size
    }

    fun addAllIssues(symbolDtos: List<IssueDto>) {

        symbolDtos.forEachIndexed { index, issueDto ->
            if (index == 0) {
                if (mTitleList.isEmpty()) {
                    mTitleList.addAll(issueDto.toList())
                }
            } else {
                if (!mItems.contains(issueDto)) {
                    mItems.add(issueDto)
                    notifyItemInserted(index)
                }
            }
        }
    }

}