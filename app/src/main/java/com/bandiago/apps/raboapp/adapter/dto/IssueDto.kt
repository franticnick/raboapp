/*
 * Copyright (c) 2018 Bandiago
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Dto class for parsed records
 */
package com.bandiago.apps.raboapp.adapter.dto


data class IssueDto(
    var v1: String,
    var v2: String,
    var v3: String,
    var v4: String
) {
    fun toList(): List<String> {
        return arrayListOf(v1, v2, v3, v4)
    }

}