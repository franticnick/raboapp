/*
 * Copyright (c) 2018 Bandiago
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bandiago.apps.raboapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bandiago.apps.raboapp.adapter.dto.IssueDto
import com.bandiago.apps.raboapp.repository.IssuesRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


/**
 * View model
 */
class IssuesViewModel(private val issuesRepository: IssuesRepository) : ViewModel() {

    /**
     * Get all issues from the file
     */
    fun getIssues(): LiveData<List<IssueDto>> {
        val result = MutableLiveData<List<IssueDto>>()

        GlobalScope.launch(Dispatchers.Main) {
            result.value = issuesRepository.getIssues()
        }

        return result
    }

}
