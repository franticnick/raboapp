
/*
 * Copyright (c) 2018 Bandiago
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bandiago.apps.raboapp.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bandiago.apps.raboapp.R
import com.bandiago.apps.raboapp.adapter.IssuesAdapter
import com.bandiago.apps.raboapp.adapter.dto.IssueDto
import com.bandiago.apps.raboapp.viewmodel.IssuesViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {

    private val mIssuesAdapter: IssuesAdapter by inject()
    private val mIssuesViewModel: IssuesViewModel by inject()

    private lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        recyclerView = findViewById(R.id.issues_list)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = mIssuesAdapter

        fab.setOnClickListener {
            finish()
        }

        getAllIssues()
    }

    private fun getAllIssues() {
        mIssuesViewModel.getIssues().observe(this, Observer<List<IssueDto>> { issuesDtos ->
            if (issuesDtos != null) {
                mIssuesAdapter.addAllIssues(issuesDtos)
            }
        })

    }
}
