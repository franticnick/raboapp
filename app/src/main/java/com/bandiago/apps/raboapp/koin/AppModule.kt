
/*
 * Copyright (c) 2018 Bandiago
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bandiago.apps.raboapp.koin

import com.bandiago.apps.raboapp.adapter.IssuesAdapter
import com.bandiago.apps.raboapp.repository.IssuesRepository
import com.bandiago.apps.raboapp.repository.reader.IssuesCsvReader
import com.bandiago.apps.raboapp.viewmodel.IssuesViewModel
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val appModule = module {
    single { IssuesAdapter() }
    single { IssuesCsvReader(get()) }
    single { IssuesRepository(get())}
    viewModel { IssuesViewModel(get())}
}