/*
 * Copyright (c) 2018 Bandiago
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bandiago.apps.raboapp

import com.bandiago.apps.raboapp.adapter.IssuesAdapter
import com.bandiago.apps.raboapp.repository.IssuesRepository
import com.bandiago.apps.raboapp.repository.reader.IssuesCsvReader
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.runner.RunWith
import org.koin.standalone.get
import org.koin.test.AutoCloseKoinTest
import org.robolectric.RobolectricTestRunner


@RunWith(RobolectricTestRunner::class)
class RobolectricTest : AutoCloseKoinTest() {

    private val issuesRepository = get<IssuesRepository>()
    private val issuesAdapter = get<IssuesAdapter>()
    private val issuesCsvReader = get<IssuesCsvReader>()

    @Test
    fun `test adapter`() {

        runBlocking  {
            val list = issuesRepository.getIssues()
            issuesAdapter.addAllIssues(list)

            assertEquals(4, issuesAdapter.mTitleList.size)
            assertEquals(list[0].toList(), issuesAdapter.mTitleList)

            assertEquals(3, issuesAdapter.mItems.size)
            assertEquals(list[1].toList(), issuesAdapter.mItems[0].toList())
            assertEquals(list[2].toList(), issuesAdapter.mItems[1].toList())
            assertEquals(list[3].toList(), issuesAdapter.mItems[2].toList())
        }
    }

    @Test
    fun `test csv reader`() {

        runBlocking  {
            val list = issuesCsvReader.getIssues()
            assertEquals(4, list.size)
        }
    }

}
