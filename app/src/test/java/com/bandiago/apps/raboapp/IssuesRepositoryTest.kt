
/*
 * Copyright (c) 2018 Bandiago
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bandiago.apps.raboapp

import com.bandiago.apps.raboapp.adapter.dto.IssueDto
import com.bandiago.apps.raboapp.repository.IssuesRepository
import com.bandiago.apps.raboapp.repository.reader.IssuesCsvReader
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
class IssuesRepositoryTest {

    private val testRow1 = arrayOf("0", "1", "2", "3")
    private val testRow2 = arrayOf("a", "b", "c", "d")

    @MockK
    private lateinit var mockCsvReader: IssuesCsvReader

    lateinit var issuesRepository: IssuesRepository


    @Before
    fun init() {
        MockKAnnotations.init(this, relaxUnitFun = true)

        coEvery { mockCsvReader.getIssues() } returns listOf(testRow1, testRow2)
        issuesRepository = IssuesRepository(mockCsvReader)
    }

    @Test
    fun `test mocked repository`() = runBlocking {

        val testResult = issuesRepository.getIssues()
        assertEquals(IssueDto(testRow1[0], testRow1[1], testRow1[2], testRow1[3]), testResult[0])
        assertEquals(IssueDto(testRow2[0], testRow2[1], testRow2[2], testRow2[3]), testResult[1])

    }
}
